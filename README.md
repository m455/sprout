# sprout

a tool that manages an rss-only blog.

probably the ugliest code base, interface, and documentation you'll ever see.

## page contents

<!-- vim-markdown-toc GFM -->

* [what's a weed?](#whats-a-weed)
    * [what's a web feed?](#whats-a-web-feed)
    * [why not just make a regular blog post?](#why-not-just-make-a-regular-blog-post)
    * [why over RSS?](#why-over-rss)
* [requirements](#requirements)
* [installing sprout](#installing-sprout)
    * [installing sprout: for server owners](#installing-sprout-for-server-owners)
    * [installing sprout: for tilde town users](#installing-sprout-for-tilde-town-users)
* [configuring sprout](#configuring-sprout)
    * [configuring sprout: for server owners](#configuring-sprout-for-server-owners)
        * [configuring your server](#configuring-your-server)
        * [setting up sprout on your local machine](#setting-up-sprout-on-your-local-machine)
    * [configuring sprout: for tilde town users](#configuring-sprout-for-tilde-town-users)
* [todos](#todos)

<!-- vim-markdown-toc -->

## what's a weed?

"weed" is a transformation of the word "web feed", kind of like how "blog" is a
transformation of "web log". the term was originally coined by
[~dozens](http://tilde.town/~dozens).

### what's a web feed?

an rss feed.

### why not just make a regular blog post?

the internet can be daunting nowadays, especially with how data is being sold.
this makes being yourself online a little more scarier than how things were in
the past.

by limiting access to your content by making it rss-only, it's a little less
scary (for me anyway). it's not private, but it's also not as public as
something that someone can just browse to with an internet browser.

### why over RSS?

so many readers for rss exist nowadays, and it's a great way to keep things
decentralized.

## requirements

* racket
* rsync

## installing sprout

sprout generates an rss feed of of your blog posts, and can publish them to a
local or remote location. you can choose where you you publish your posts.

following one of the sections below:

* [installing sprout: for server owners](#installing-sprout-for-server-owners)
* [installing sprout: for tilde town users](#installing-sprout-for-tilde-town-users)

### installing sprout: for server owners

this section guides you through setting up sprout on your local machine. the
setup installs sprout into `/usr/local/bin`.

follow the steps below to install sprout:

1. run `git clone git://git.m455.casa/sprout`
2. run `cd sprout`
3. run `make install` as root

### installing sprout: for tilde town users

this section guides you through setting up sprout on tilde.town. the setup
installs sprout into `~/.local/bin`.

follow the steps below to install sprout:

1. run `git clone git://git.m455.casa/sprout`
2. run `cd sprout`
3. run `mkdir -p ~/.local/bin`
3. run `make install DESTDIR=~/.local/bin`

## configuring sprout

sprout comes with a default configuration file that you need to change,
depending on your use case.

following one of the sections below:

* [configuring sprout: for server owners](#configuring-sprout-for-server-owners)
* [configuring sprout: for tilde town users](#configuring-sprout-for-tilde-town-users)

### configuring sprout: for server owners

you need to set up your server, and then configure sprout, so you can
publish your posts to your server.

this section assumes you have the following on your server:

* nginx
* systemd
* root access to your server

#### configuring your server

follow the steps below as the root user:

1. run `mkdir -p /var/www/weed`
2. run `chown -R username:username /var/www/weed`
3. run `chmod -R 755 /var/www/weed`
4. add the following to your `server {}` block in
   `/etc/nginx/sites-available/example.com`:

   ```
   location /weed {
       alias /var/www/weed;
       index weed.rss;
   }
   ```

5. run `ln -s /etc/nginx/sites-available/example.com
   /etc/nginx/sites-enabled/example.com`
6. as root, run `systemctl restart nginx`

#### setting up sprout on your local machine

sprout creates the following directory structure in your home directory:

```
.weed/
├── build/
├── config/
└── posts/
```

follow the steps below on your local machine:

1. run `sprout`
2. agree to the setup
3. type `s`
4. change the `title` value to the title you want
5. change the `description` value to the title you want
6. change the `url-base` value to the URL that you want others to access your
   weed from

   for example: `(url-base "https://example.com/weed/")`

7. change the `output-path` to where you want your weed to publish remotely

   for example: `(output-path "username@example.com:/var/www/weed/")`

8. save the file

**note**: make sure you include a slash at the end of the `url-base` and
`output-path` values.

### configuring sprout: for tilde town users

you need to set up your `~/public_html` directory on tilde.town, and then
configure sprout, so you can publish your posts to tilde.town.

sprout creates the following directory structure in your home directory:

```
.weed/
├── build/
├── config/
└── posts/
```

follow the steps below to set up sprout on tilde.town:

1. run `mkdir ~/public_html/weed`
1. run `sprout`
2. agree to the setup
3. type `s`
4. change the `title` value to the title you want
5. change the `description` value to the title you want
6. change the `url-base` value to the URL that you want others to access your
   weed from

   for example: `(url-base "https://tilde.town/~username/weed/")`

7. change the `output-path` to where you want your weed to publish locally

   for example: `(output-path "~/public_html/weed/")`

8. save the file

**note**: make sure you include a slash at the end of the `url-base` and
`output-path` values.

## todos

* implement pagination
