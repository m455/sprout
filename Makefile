PROGRAM = sprout
DESTDIR ?= /usr/local/bin
LOCALDIR = ~/.local/bin

.PHONY: install
install: build
	chmod ugo+x ./$(PROGRAM)
	mv ./$(PROGRAM) $(DESTDIR)/$(PROGRAM)

.PHONY: uninstall
uninstall: $(DESTDIR)/$(PROGRAM)
	rm $(DESTDIR)/$(PROGRAM)

build: $(PROGRAM).rkt
	raco exe -o ./$(PROGRAM) $(PROGRAM).rkt

.PHONY: clean
clean: ./$(PROGRAM)
	rm ./$(PROGRAM)
